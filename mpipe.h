//-*-c++-*-
#pragma once

#include <thread>
#include <vector>
#include <cassert>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/types.h>

// the real pipe class
struct mpipe_impl {
    static const size_t PIPE_SIZE = 256*1024;
    
    // pipe handle, can't store int[2] in a vector =(
    struct pipefd {
        pipefd(int read, int write)
            : nwrote(0), read(read), write(write) {}

        ssize_t nwrote;
        int read;
        int write;
    };

    
    // default (and only constructor)
    mpipe_impl()
        : wpipe(make_pipe()), paused(false), running(false), dirty(false) {
    }

    
    // cleanup and close all pipes
    ~mpipe_impl() {
        stop();
    }

    
    // return write handle    
    int writer() {
        return wpipe.write;
    }

    
    // add a reader to the pipe
    int add_reader() {
        assert(!dirty && "attempted to attach reader to dirty pipe");
        rpipes.push_back(
            make_pipe()
        );
        return rpipes.back().read;
    }

    
    // start pipe streaming, can never add readers again
    void start() {
        assert(!dirty && "attempted to restart dirty pipe");
        running = true;
        dirty   = true;
        thread  = std::thread(&mpipe_impl::monitor, this);
    }

    // stop pipe
    void stop() {
        if (!running) {
            return;
        }
        
        running = false;
        if (thread.joinable()) {
            thread.join();
        }

        // close our ends of pipes, everybody else should close theirs
        close(wpipe.read);
        close(wpipe.write);
        for (auto fd : rpipes) {
            close(fd.write);
        }
    }
    
    // pause pipe
    void pause() {
        paused = true;
    }

    // resume pipe
    void resume() {
        paused = false;
    }
    
    // write bytes to pipe
    ssize_t write(const void *data, size_t bytes) {
        size_t total = 0;
        const char* ptr = (const char*)data;
        while (bytes > 0) {
            int wrote = ::write(wpipe.write, ptr, bytes);
            if (wrote < 0) {
                break;
            }
            bytes -= wrote;
            total += wrote;
            ptr   += wrote;
        }
        return total;
    }

    // write bytes to pipe
    ssize_t read(int readfd, void *data, size_t bytes) {
        size_t total = 0;
        char* ptr = (char*)data;
        while (bytes > 0) {
            int read = ::read(readfd, ptr, bytes);
            if (read < 0) {
                break;
            }
            bytes -= read;
            total += read;
            ptr   += read;
        }
        return total;
    }
    
private:
    // tee data from incoming FIFO to outgoing reader FIFOs.
    // tee()-ing data is very efficient as the kernel simple moves pipe buffer
    // references around under the hood.
    bool tee_data(pipefd wpipe, std::vector<pipefd> &rpipes, int nullfd, int nfd, bool flush) { 
        // tee() data into the read pipes.  We take any pipes that are empty
        // and tee the data in.  We add the amount teed to their write count
        // and look for the minimum.  That value is how much we can take out
        // of the pipe safely, which we do by splicing to /dev/null
        //
        // when we get the flush signal, we loop until we don't write anymore data
        bool some_left;
        bool update_epoll = false;
        do {
            int64_t min_val = INT64_MAX;
            for (ssize_t ii=rpipes.size()-1; ii >= 0; ii--) {
                int64_t *nwrote = &rpipes[ii].nwrote;
                int      wfd    =  rpipes[ii].write;

                if (*nwrote == 0) {
                    int nteed = tee(wpipe.read, wfd, PIPE_SIZE, SPLICE_F_NONBLOCK | SPLICE_F_MORE | SPLICE_F_MOVE);

                    // an EPIPE indicates reader is dead, so remove it
                    if (nteed < 0 && errno == EPIPE) {
                        printf("error, closing reader %zd\n", ii);
                        close(rpipes[ii].write);
                        rpipes.erase(rpipes.begin()+ii);
                        update_epoll = true;
                        continue;  // return to top of loop and try again
                    }

                    if (nteed < 0) {
                        nteed = 0;
                    }

                    *nwrote += nteed;
                }

                // update minval
                if (*nwrote < min_val) {
                    min_val = *nwrote;
                }
            }

            // remove min_val from pipe
            if (min_val > 0) { 
                splice(wpipe.read, NULL, nullfd, NULL, min_val, 0);
            }

            // and decrement counters
            some_left = false;
            for (size_t ii=0; ii < rpipes.size(); ii++) {
                int64_t *nwrote = &rpipes[ii].nwrote;
                *nwrote   -= min_val;
                some_left |= (*nwrote > 0);
            }
        } while ((flush && some_left) || --nfd > 0);
    }
    

    // monitor thread, pushes data between pipes
    void monitor() {
        // structure to hold epoll events
        const size_t nepollevt = 64;
        struct epoll_event events[nepollevt];

        // create epoll file descriptors
        int wepoll = epoll_create(1);  assert(wepoll != -1 && "error getting epoll handle");

        // add read end of write pipe for data coming in
        struct epoll_event ev = epoll_event();
        ev.events  = EPOLLIN;
        ev.data.fd = wpipe.read;
        epoll_ctl(wepoll, EPOLL_CTL_ADD, wpipe.read, &ev);

        // file descriptor to splice() to to remove data from pipe
        int nullfd = open("/dev/null", O_WRONLY);
        assert(nullfd != -1 && "error opening /dev/null, something awful has happened");

        bool update_epoll = true;
        int  repoll = -1;
        while (running) {
            if (paused) {
                usleep(10000); // 10ms
                continue;
            }

            // update reader epoll if needed
            if (update_epoll) {
                update_epoll = false;
                if (repoll != -1) {
                    close(repoll);
                }

                repoll = epoll_create(1);  assert(repoll != -1 && "error getting epoll handle");

                for (ssize_t ii=0; ii < rpipes.size(); ii++) {
                    ev.events  = EPOLLOUT;
                    ev.data.fd = rpipes[ii].write;
                    epoll_ctl(repoll, EPOLL_CTL_ADD, rpipes[ii].write, &ev);
                }
            }

            // wait for writer to become ready
            int nwfd = epoll_wait(wepoll, events, 1, 1000);

            // check for hangup on write file descriptor
            if (events[0].events & (EPOLLERR | EPOLLHUP | EPOLLRDHUP)) {
                break;
            }

            // wait for at least one reader to become ready
            int nrfd = epoll_wait(repoll, events, nepollevt, 1000);
            if (nrfd == 0) continue;

            // tee some data between writer and reader pipe
            update_epoll = tee_data(wpipe, rpipes, nullfd, nrfd, false);
            
            // if all readers exited, quit
            if (rpipes.size() == 0) {
                break;
            }
        }

        // flush remaining data
        tee_data(wpipe, rpipes, nullfd, 1, true);

        // stop and close pipes
        stop();
        
        // destroy epoll instances
        close(wepoll);
        close(repoll);
    }

    // create a pipe handle
    static pipefd make_pipe() {
        int fds[2];
        assert(pipe(fds) == 0 && "error opening pipe, something terrible has happened");
#ifdef F_SETPIPE_SZ
        fcntl(fds[0], F_SETPIPE_SZ, PIPE_SIZE);
#endif
        return pipefd(fds[0], fds[1]);
    }

    std::thread thread;         // monitor thread
    bool paused;                // are we paused?
    bool running;               // are we streaming right now?
    bool dirty;                 // can we add readers?
    pipefd wpipe;               // write pipe 
    std::vector<pipefd> rpipes; // reader pipes
}; 



// instantiate this
struct mpipe {
    mpipe()
        : pimpl(std::make_shared<mpipe_impl>()) {}

    int  writer()     { return pimpl->writer();     }
    int  add_reader() { return pimpl->add_reader(); }
    void start()      { pimpl->start();             }
    void stop()       { pimpl->stop();              }
    void pause()      { pimpl->pause();             }
    void resume()     { pimpl->resume();            }
    
    // write bytes to pipe
    ssize_t write(const void *data, size_t bytes) {
        return pimpl->write(data, bytes);
    }

    // read bytes from pipe
    ssize_t read(int readfd, void *data, size_t bytes) {
        return pimpl->read(readfd, data, bytes);
    }
        
private:
    std::shared_ptr<mpipe_impl> pimpl;
};      
