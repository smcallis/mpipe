#ifndef NONSTDIO_H_
#define NONSTDIO_H_

// This file contains functions wrapping the standard library functions

#include <errno.h>
#include <stdint.h>
#include <unistd.h>


/*******************************************************************************
 * Allocates memory and checks for failure to allocate, thus making
 * memory allocation safe.
 *
 * @param  bytes  number of bytes to allocate
 * @return pointer to memory, never returns NULL
 *
 *******************************************************************************/
#define xmalloc(bytes) xmalloc_(__FILE__, __LINE__, bytes)
static inline void* xmalloc_(const char* file, unsigned line, size_t bytes) {
    void* ptr = malloc(bytes);
    if (ptr == NULL) {
        fprintf(stderr, "%s:%u -- memory allocation failed\n", file, line);
        exit(EXIT_FAILURE);
    }
    return ptr;
}


/*******************************************************************************
 * Allocates memory and checks for failure to allocate, thus making
 * memory allocation safe.  Also zeros the memory before returning.
 *
 * @param  bytes  number of bytes to allocate
 * @return pointer to memory, never returns NULL
 *
 *******************************************************************************/
#define xcalloc(nmemb, bytes) xcalloc_(__FILE__, __LINE__, nmemb, bytes)
static inline void* xcalloc_(const char* file, unsigned line, size_t nmemb, size_t bytes) {
    void* ptr = calloc(nmemb, bytes);
    if (ptr == NULL) {
        fprintf(stderr, "%s:%u -- memory allocation failed\n", file, line);
        exit(EXIT_FAILURE);
    }
    return ptr;
}


/********************************************************************************
 *
 * @brief reads data from a file descriptor
 * @param fd - file descriptor
 * @param ptr - buffer to read the data into
 * @param len - length in bytes to read
 * @return the number of bytes read
 *
 * The standard read(...) function can be interupted or return early for several
 * reasons.  Refer to the man page for details.  This routines checks these
 * conditions and will retry reading as appropriate to try and satisfy the full
 * request.
 *
 ********************************************************************************/
static inline size_t xread (int fd, void* ptr, size_t len) {
    char* buf = (char*)ptr;
    size_t total = 0;
    while (len) {
        ssize_t got = read(fd, buf, len);
        if (got <= 0) break;
        len -= got;
        total += got;
        buf += got;
    }
    return total;
}


/********************************************************************************
 *
 * @brief writes data to a file descriptor
 * @param fd - file descriptor
 * @param ptr - buffer to writed the data from
 * @param len - length in bytes to write
 * @return the number of bytes written
 *
 * The standard write(...) function can be interupted or return early for several
 * reasons.  Refer to the man page for details.  This routines checks these
 * conditions and will retry writing as appropriate to try and satisfy the full
 * request.
 *
 ********************************************************************************/
static inline size_t xwrite (int fd, const void* ptr, size_t len) {
    const char* buf = (const char*)ptr;
    size_t total = 0;
    while (len) {
        ssize_t put = write(fd, buf, len);
        if (put < 0) break;
        len -= put;
        total += put;
        buf += put;
    }
    return total;
}


/********************************************************************************
 *
 * @brief skip bytes in a stream or file
 * @param fd - the file descriptor to skip bytes in
 * @param bytes - the number of bytes to skip
 *
 * This routine is similar in spirit to the standard C lseek(...) and fseek(...) functions.
 * This routine is different though in that it only supports skipping forward in the file
 * descriptor, and it also supports skipping through streaming (piping or socket) data.
 *
 ********************************************************************************/
static inline int xskip (int fd, uint64_t bytes) {
    if (bytes == 0) return 1;
    off_t result = lseek(fd, bytes, SEEK_CUR);
    if (result == (off_t)-1) {
        if (errno != ESPIPE) return 0;
        // Can't quickly seek, so we'll manually dump data
        static char ignored[8192];
        while (bytes) {
            uint64_t amt = bytes;
            if (amt > 8192) amt = 8192;
            uint64_t got = xread(fd, ignored, amt);
            if (got != amt) return 0;
            bytes -= got;
        }
        return 1;
    }
    return 1;
}

#endif//NONSTDIO_H_
