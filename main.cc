#include <mpipe.h>

#include <time.h>
#include <string.h>

#include <nonstdlib.h>

// stopwatch
double stopwatch() {
    thread_local timespec last;
    timespec curr;
    clock_gettime(CLOCK_MONOTONIC, &curr);
    double elapsed = (double)(curr.tv_sec - last.tv_sec) + (double)(curr.tv_nsec - last.tv_nsec)*1e-9;
    last = curr;
    return elapsed;
}


void speed_reader(int rfd) {
    ssize_t bytes = 0;
    char*   data  = (char*)malloc(1024*1024);

    stopwatch();
    for (;;) {
        int nread = read(rfd, data, 1024*1024);
        if (nread <= 0) {
            break;
        }
        bytes += nread;
    }
    double secs = stopwatch();
    printf("read %zd bytes in %.3f seconds (%.3f MB/s)\n", bytes, secs, bytes/1024.0/1024.0/secs);
    close(rfd);    
    free(data);
}


static char   *data  = NULL;
static ssize_t dsize = 1024*1024;

void check_reader(int rfd) {
    char* ldata = (char*)malloc(dsize);
    for (;;) {
        int nread = xread(rfd, ldata, dsize);
        if (nread == 0) {
            break;
        }

        if (nread < dsize) {
            printf("only read %d bytes!\n", nread);
            free(ldata);
            return;
        }

        if (memcmp(ldata, data, dsize) != 0) {
            printf("data error when reading\n");
            free(ldata);
            return;
        }
    }
    printf("data read OK\n");
}


int main() {
    printf("[speed test]\n");
    { 
        mpipe pipe;        
        std::thread aa(speed_reader, pipe.add_reader());
        std::thread bb(speed_reader, pipe.add_reader());
        std::thread cc(speed_reader, pipe.add_reader());
        std::thread dd(speed_reader, pipe.add_reader());
        pipe.start();

        uint64_t iters = 32ull*1024ull*1024ull*1024ull;
        iters /= dsize;
        char*  data  = (char*)malloc(dsize);
        for (ssize_t ii=0; ii < iters; ii++) {
            if (write(pipe.writer(), data, dsize) == 0) {
                break;
            }
        }
        pipe.stop();

        aa.join();
        bb.join();
        cc.join();
        dd.join();
    }

    printf("\n");
    printf("[consistency test]\n");
    { 
        mpipe pipe;        
        std::thread aa(check_reader, pipe.add_reader());
        std::thread bb(check_reader, pipe.add_reader());
        std::thread cc(check_reader, pipe.add_reader());
        std::thread dd(check_reader, pipe.add_reader());
        pipe.start();

        data = (char*)malloc(dsize);
        for (ssize_t jj=0; jj < dsize; jj++) {
            data[jj] = rand() % 8;
        }
        
        for (ssize_t ii=0; ii < 64; ii++) {
            if (write(pipe.writer(), data, dsize) < dsize) {
                printf("write error!\n");
                break;
            }
        }
        pipe.stop();

        aa.join();
        bb.join();
        cc.join();
        dd.join();

        free(data);        
    }

    
}

